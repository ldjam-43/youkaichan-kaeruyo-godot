extends Camera2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_player_move():
	var player = get_parent().get_node('player')
	var transform = player.get_global_transform()
	var viewport = get_viewport_rect()
	offset = transform[2] - (viewport.size / 2)
