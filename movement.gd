extends KinematicBody2D

export (int) var speed = 200

signal move

func _physics_process(delta):
    var velocity = Vector2()
    if Input.is_action_pressed('ui_right'):
        velocity.x += 1
    if Input.is_action_pressed('ui_left'):
        velocity.x -= 1
    if Input.is_action_pressed('ui_down'):
        velocity.y += 1
    if Input.is_action_pressed('ui_up'):
        velocity.y -= 1
    velocity = velocity.normalized() * speed
    move_and_slide(velocity)

    var sprite = get_node('sprite')
    if velocity.x > 0:
      sprite.set_flip_h(1)
    elif velocity.x < 0:
      sprite.set_flip_h(0)

    if velocity.x != 0 || velocity.y != 0:
      emit_signal('move')
